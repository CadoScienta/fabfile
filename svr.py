from fabric.api import *
import socket
import re
import sys

@task(default=True)
@serial
@hosts('localhost')
def systems(hostsfile="hosts"):
    """Uses supplied file to determine hosts. Creates list of unreachable hosts."""
    badhosts = []
    try:
        hostnames = open('%s' % (hostsfile), 'r').readlines()
    except:
        print('No %s file found...' % (hostsfile))
    else:
        for hostname in hostnames:
            if not hostname.startswith("#"):
                s = socket.socket()
                s.settimeout(1)
                try:
                    s.connect((hostname.strip(), 22))
                    env.hosts.append(hostname.strip())
                except socket.error as e:
                    badhosts.append(hostname.strip())
                s.settimeout(None)
    if badhosts:
        badhostsfile = hostsfile + "_badhosts"
        badfile = open(badhostsfile,'w')
        for hostname in  badhosts:
            print(hostname, file=badfile)

@task
def porttest(port):
    """Tests a simple socket connection to a port on the remote host"""
    s = socket.socket()
    s.settimeout(1)
    print(env.host_string),
    try:
        s.connect((env.host_string, int(port)))
        print(': Success')
    except socket.error as e:
        print(': Failure')

@task
def remoteport(host,port=443):
    """Connect to host and then test connection to another host on specified port"""
    script="/tmp/porttest.py"
    command=""" echo '#!/usr/bin/env python\n\n\
import socket,re,sys\n\
port=%s\n\
s = socket.socket()\n\
try:\n\
    s.connect(("%s", int(port)))\n\
    print "%s:%s : Success"\n\
except socket.error, e:\n\
    print "%s:%s : Failed: %%s" %% (e)\n\
s.close()' > %s\n """ % (port,host,host,port,host,port,script)
    run(command)
    run('chmod 755 %s' % script)
    run(script)
    run('rm %s' % script)

@task
def mailer(toAddress):
    """Test if mail is being sent out correctly"""
    run("echo 'This is a test message' | mail -s Test %s" % (toAddress))

