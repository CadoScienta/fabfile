from fabric.api import *
from fabric.contrib import files
#from .lx import *

@task(default=True)
def entrust():
    """Will install and configure Entrust token software"""
    if run( 'rpm -q entrust-pam-plugin' ).return_code:
        lx.updaterpm("entrust-pam-plugin-10.1-0.x86_64.rpm")
    execute(config)

@task
def config():
    """Configures SSH and PAM to use Entrust tokens"""
    if files.exists('/etc/pam.d/sshd',use_sudo=True):
        sudo('cp /etc/pam.d/sshd /etc/pam.d/sshd.bak')
    put('sshd','/etc/pam.d/sshd',use_sudo=True,mode="0644")
    if not files.exists('/etc/raddb',use_sudo=True):
        sudo('mkdir /etc/raddb')
    if files.exists('/etc/raddb/server',use_sudo=True):
        sudo('cp /etc/raddb/server /etc/raddb/server.old')
    put('server','/etc/raddb/server',use_sudo=True,mode='0600')
    files.sed('/etc/ssh/sshd_config','^.*ChallengeResponseAuthentication no','ChallengeResponseAuthentication yes',use_sudo=True)
    sudo('service sshd restart')

