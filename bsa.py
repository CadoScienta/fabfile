from fabric.api import *
from fabric.contrib import files

@task
def bsainstall():
    """Runs the shell installer for BSA"""
    run('mkdir /tmp/bsainstall')
    with cd('/tmp/bsainstall'):
        sudo('firewall-cmd --zone=public --add-port=4750/tcp --permanent')
        sudo('firewall-cmd --reload')
        run('wget http://oirrhstpvxw02.edc.ds1.usda.gov/bsa/RSCD89-SP1-LIN64.sh')
        run('wget http://oirrhstpvxw02.edc.ds1.usda.gov/bsa/nsh-install-defaults')
        run('wget http://oirrhstpvxw02.edc.ds1.usda.gov/bsa/users')
        run('chmod 755 RSCD89-SP1-LIN64.sh')
        sudo('./RSCD89-SP1-LIN64.sh -silent')
        sudo('mv -f users /etc/rsc')
    sudo('rm -rf /tmp/bsainstall')
    sudo('chmod 600 /etc/rsc/users')

@task
def bsaexports():
    """Adds a generic exportss file for BSA"""
    with cd('/etc/rsc'):
        put('exports','exports','mode=0640',use_sudo=True)
        sudo('chown root.root exports')
