from fabric.api import *

@task
@serial
def set(username):
    """Set username to use on remote systems"""
    env.user = username

@serial
def pwprompt():
    """Force fabric to prompt for password before it needs one"""
    import getpass
    env.password = getpass.getpass('Password: ')

@serial
def password():
    """Try to read password from a file then prompt if file not found"""
    from os.path import expanduser
    home =  expanduser("~")
    pwdfile = "%s/fabfile/.password" % (home)
    try:
        with open(pwdfile, 'r') as r:
            env.password = r.readline().strip()
    except:
        pwprompt()

@task
def sshkey(keyfile='~/.ssh/keys'):
    """Install user ssh public key on the server using key loaded in agent"""
    run('[ -d .ssh ] || mkdir .ssh')
    run('chmod 700 .ssh')
    run('[ -f .ssh/authorized_keys2 ] && rm -f .ssh/authorized_keys2')
    run('[ -f .ssh/authorized_keys ] && chmod 600 .ssh/authorized_keys')
    put('%s' % (keyfile),'.ssh/authorized_keys')
    run('chmod 400 .ssh/authorized_keys')

