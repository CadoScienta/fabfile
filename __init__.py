import logging
from fabric.api import *
from .base import *
from .svr import *
from . import user
from . import bsa
from . import lx
from . import rep
from . import compliance
from . import entrust
from . import beta

logging.basicConfig()

env.skip_bad_hosts = True
env.warn_only = True
env.use_ssh_config = True
env.colorize_errors = True
env.forward_agent  = True
env.pool_size = 50
env.parallel = True
env.colorize_errors = True
env.abort_on_prompts = True
env.prompts = {'Enter your PVN and a response from your token with serial number 31346-03541.':' '}
#env.gateway = "oirjbox0003"

output['aborts'] = True
output['debug'] = False
output['running'] = False
output['status'] = False
output['stderr'] = True
output['stdout'] = True
output['user'] = True
output['warnings'] = False

# This will try to read the password from a file, then prompt if it fails.
if not env.password:
    user.password()
