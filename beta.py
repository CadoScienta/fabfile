from fabric.api import *
from fabric.contrib import files

@task
def partytime():
    """Just a filler"""
    run('echo "$(hostname) is here to party"')

@task
def dockerls():
    """List containers running on server"""
    sudo('docker container ls')

