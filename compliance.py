from fabric.api import *
from fabric.contrib import files

@task
@serial
@hosts('localhost')
def entrust():
    """Will check Entrust radius configuration"""
    execute(entrust_header)
    execute(entrust_data)

@serial
@hosts('localhost')
def entrust_header():
    """Prints header for CSV file"""
    print("Hostname, RPM Installed, raddb File, raddb Configured, PAM sshd, PAM Configured, SSHd Configured")

@hosts(env.hosts)
def entrust_data():
    """Checks all aspects of entrust configuration"""
    output['everything'] = False
    configfile="/etc/raddb/server"
    confoptions=['OIRACTLPVWA01.edc.ds1.usda.gov:1812','OIRACTLPVWA02.edc.ds1.usda.gov:1812']
    pamfile="/etc/pam.d/sshd"
    pamtxt="pam_radius_auth.so"
    sshd_config="/etc/ssh/sshd_config"
    sshcheck="^ChallengeResponseAuthentication yes"

    print(env.host_string),
    rpmstatus = run( 'rpm -q entrust-pam-plugin' )
    if rpmstatus.return_code:
        print(", no"),
    else:
        print(", yes"),
    if files.exists(configfile,use_sudo=True):
        print(", yes"),
        conftest=", yes"
        for confoption in confoptions:
            if not files.contains(configfile,confoption,use_sudo=True):
                conftest=", no"
        print(conftest),
    else:
        print(", no, n/a"),
    if files.exists(pamfile,use_sudo=True):
        print(", yes"),
        if files.contains(pamfile,pamtxt,use_sudo=True):
            print(", yes"),
        else:
            print(", no"),
    else:
        print(", no, n/a"),
    if files.contains(sshd_config, sshcheck,use_sudo=True,escape=False):
        print(", yes"),
    else:
        print(", no"),
    print()
