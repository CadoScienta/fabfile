from fabric.api import *
from fabric.contrib import *

@task
def date():
    """Runs date on the remote server"""
    run('date')

@task
def runcmd(cmd,asroot=True):
    """Runs a command"""
    if asroot:
        runit=sudo
    else:
        runit=run
    runit('%s' % (cmd))

@task
def runas(cmd,name,usesudo=False):
    """Runs a command as a specific user(root default) using su as root by default"""
    if name:
        if usesudo:
            sudo("%s" % (cmd), user="%s" % (name))
        else:
            sudo('su - %s -c "%s"' % (name,cmd))
    else:
        print("Please provide a user to run the command as")

@task
def runargs(cmd,argsfile,asroot=False):
    """Run command on remote host looping through a list of arguments"""
    import sys
    import re

    if asroot:
        runit=sudo
    else:
        runit=run
    try:
        args = open('%s' % (argsfile), 'r').readlines()
    except:
        print('No %s file found...' % (argsfile))
    else:
        for arg in args:
            if not arg.startswith("#"):
                runit('%s %s' % (cmd,arg))

@task
def runscript(scriptfile,args=""):
    """Runs the local script file on the remote servers using sudo"""
    put('%s' % (scriptfile),'/tmp/%s' % (scriptfile))
    run('chmod 755 /tmp/%s' % (scriptfile))
    sudo('/tmp/%s %s' % (scriptfile,args))
    run('rm /tmp/%s' % (scriptfile))

@task
def service(name,args="status"):
    """Init service management"""
    if files.exists("/sbin/service",use_sudo=True) == True:
        sudo("/sbin/service %s %s" % (name,args))
    else:
        if files.exists("/etc/init.d/%s" % (name), user_sudo=True) == True:
            sudo("/etc/init.d/%s %s" % (name,args))
        else:
            print("Service %s was not found\n" % (name))

@task
def sudorule(sudofile,rule):
    """Adds rule to the sudofile specified"""
    sudo("echo %s >> /etc/sudoers.d/%s" % (rule,sudofile))
@task

def sudoeradd(file):
    """Add a file to sudoers.d"""
    import os.path
    BASENAME=os.path.basename('%s' % (file))
    put('%s' % (file),'/tmp','mode=0440')
    sudo('chown root.root /tmp/%s' % (BASENAME))
    sudo('mv /tmp/%s /etc/sudoers.d' % (BASENAME))

@task
def userscript(scriptfile,args=""):
    """Runs the local script file on the remote servers as your user"""
    put('%s' % (scriptfile),'/tmp/%s' % (scriptfile))
    run('chmod 755 /tmp/%s' % (scriptfile))
    run('/tmp/%s %s' % (scriptfile,args))
    run('rm /tmp/%s' % (scriptfile))

@task
def getfile(remfile):
    """gets remote file"""
    get('%s' % (remfile),'%s/%s' % (env.host,remfile))

@task
@serial
def testhost():
    """Run the hostname command as your user and using sudo"""
    #UNAME = run('uname')
    #if(UNAME == "SunOS"):
    #    env.sudo_prefix = "/usr/local/bin/sudo -S -p '%(sudo_prompt)s' "
    #else:
    #    env.sudo_prefix = "sudo -S -p '%(sudo_prompt)s' "
    run('echo "$HOSTNAME: $USER"')
    sudo('echo "$HOSTNAME: $USER"')

@task
def dummy():
    """Prints hostname to verify fabric is working"""
    print(env.host_string)
