from fabric.api import *
from fabric.contrib import files

# need to check if all still valide for RHEL 6,7,8

@task
def addinit(initfile):
    """Add and configure supplied init file"""
    put('%s' % (initfile),'/tmp/%s' % (initfile))
    sudo('mv /tmp/%s /etc/init.d' % (initfile))
    sudo('chmod 755 /etc/init.d/%s' % (initfile))
    sudo('chkconfig --add %s' % (initfile))
    sudo('chkconfig %s on' % (initfile))

@task
def fixrpmdb():
    """Fix basic RPM database error"""
    sudo("mv /var/lib/rpm/Pubkeys /tmp")
    sudo("rpm --rebuilddb")

@task
def ro():
    """Check for readonly filesystems in linux"""
    run('egrep " ro,|,ro " /proc/mounts')

@task
def checkrpm(package):
    """Provide information about package"""
    run ('rpm -qi %s' % (package))

@task
def rhversion(outfile = ""):
    """Print the current version of RHEL and the architecture. Arg will write out to CSV file of the arg"""
    if not outfile:
        output['everything'] = False
        output['status'] = False
        output['user'] = True
    version = get_linux_info("version")
    arch = get_linux_info("arch")
    if not outfile:
        from fabric.colors import red,green
        puts(red("%s") % (version) + green(" %s") % (arch))
    else:
        fileout = open(outfile,'a')
        print>>fileout,"%s,%s,%s" % (env.host,version,arch)

def get_linux_info(info="version"):
    """Commands for tasks to get information from the hosts"""
    commands = {}
    commands["version"] = "rpm -qa | grep redhat-release | grep -vi note | xargs rpm -qi | grep -i \"^version\" | awk \'{print $3}\'"
    commands["arch"] = "uname -i"
    if info == "version":
        outvalue = run(commands["version"])
    elif info == "arch":
        outvalue = run(commands["arch"])
    else:
        outvalue = ""
    return outvalue

@task
def updaterpm(rpmfile):
    """Update/install using local package"""
    put('%s' % (rpmfile), '/tmp/%s' % (rpmfile))
    sudo('rpm -Uvh --nosignature /tmp/%s' % (rpmfile))
    run('rm /tmp/%s' % (rpmfile))

@task
def freshenrpm(rpmfile):
    """Update/install using local package"""
    put('%s' % (rpmfile), '/tmp/%s' % (rpmfile))
    sudo('rpm -Fvh --nosignature /tmp/%s' % (rpmfile))
    run('rm /tmp/%s' % (rpmfile))

@task
def patch(gpgcheck=True,reboot=False):
    """Update server using yum"""
    if gpgcheck:
        flags = ""
    else:
        flags = "--nogpgcheck"
    if reboot:
        suffix = "&& reboot"
    else:
        suffix = ""
    sudo('yum clean all')
    sudo('/usr/bin/yum %s update --disablerepo=* --enablerepo=rhel*,rhn*,epel*,ius* --disablerepo=*debug*,*source*,*testing* --exclude=open-vm-tools* --skip-broken -y %s' % (flags,suffix))

@task
def checkupdate():
    """Check for available updates"""
    sudo('yum check-update')

@task
def virtualinfo():
    """Get info about virtual machines"""
    output['everything'] = False
    output['status'] = False
    output['user'] = True
    virtenv=sudo('virt-what')
    print ("%s,%s" % (env.host_string,virtenv))

@task
def bsares():
    """Restart BSA agent"""
    import time
    sudo('/sbin/service rscd stop')
    time.sleep(5)
    sudo('/sbin/service rscd start')

@task
def qpatch(task):
    """Patch using same method as BSA"""
    put('/home/prunyj01/git-repos/blpatch/patch_worker.sh','/tmp','mode=0755')
    sudo('/tmp/patch_worker.sh %s' % (task))
