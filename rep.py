from fabric.api import *
from fabric.contrib import *
from .timeout import *

@task(default=True)
@parallel(pool_size=5)
def script(name,tmout=180):
    """Run scripts to print reports"""
    output['everything'] = False
    UNAME = run('uname')
    VERSION = run('uname -r | cut -d. -f2') # Only meaningful with SunOS
    local("echo %s >> %s.started" % (env.host,name))
    if (UNAME == "SunOS") and (VERSION <= "10"):
        env.sudo_prefix = "/usr/local/bin/sudo -S -p '%(sudo_prompt)s' "
    else:
        env.sudo_prefix = "sudo -S -p '%(sudo_prompt)s' "
    with cd('/tmp'):
        put('%s' % (name),'%s' % (name),'mode=0755')
        try:
            with Timeout(int(tmout)):
                sudo('./%s >%s.out' % (name,name))
                print(sudo('cat %s.out' % (name)))
                local("echo %s >> %s.finished" % (env.host,name))
        except Timeout.Timeout:
            local("echo %s >> %s.aborted" % (env.host,name))
        sudo('rm %s %s.out' % (name,name))

