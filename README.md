| File          | Description                                                                              |
| :----------   | :--------------------------------------------------------------------------------------- |
| \__init__.py  | Sets the fabric environment up.                                                          |
| base.py       | Some basics that should work on all UX systems                                           |
| beta.py       | A place to work on new tasks or modify tasks without changing things that already work.  |
| bsa.py        | Some standard BSA tasks for managing the BSA agent                                       |
| compliance.py	| Checks for standard agents and configuration to verify minimal compliance standards      |
| entrust.py   	| Install and configure entrust agent to work with SSH                                     |
| lx.py         | Tasks that are Linux specific, and likely RedHat specific, or NITC specific.             |
| reports.py    | Runs a script and captures output with machine names. Designed for CSV style output      |
| svr.py        | Tasks for loading hosts into fabric to run other tasks against. Defaults to using a file |
| timeout.py    | A timeout class to use when a job might get stuck and you want to move along             |
| user.py       | Tasks to do user things. Set username and password. Add personal ssh key to hosts.       |
| vas.py        | Install Quest agent                                                                      |
